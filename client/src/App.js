import {ApolloClient, InMemoryCache, ApolloProvider, HttpLink, from} from '@apollo/client'
import GetItems from './GetItems';



const link = from([
  new HttpLink({uri: "http://localhost:8123/items"})
])

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: link
})

function App() {
  return (
    <ApolloProvider client={client}>
      <GetItems />
    </ApolloProvider>
    );
};

export default App;
