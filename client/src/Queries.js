import {gql} from '@apollo/client'

export const LOAD_ITEMS = gql`
query itemsQuery($page: Int, $perPage: Int){
  items(page: $page, perPage: $perPage) {
    id
    name
    absoluteIndex
  }
}
`