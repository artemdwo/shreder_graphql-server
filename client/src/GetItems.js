import React, {useEffect, useState} from "react";
import {useQuery, gql} from '@apollo/client'
import {LOAD_ITEMS} from './Queries'

function GetItems(){

    const {data, error, loading, fetchMore} = useQuery(LOAD_ITEMS, {variables: {page: 1, perPage: 100}})
    const [page, setPage] = useState(1)
    const [perPage, setPerPage] = useState(10)

    if (error) return <div>errors: {error}</div>
    if (loading || !data) return <div>loading</div>

    function updateByPage(event) {
        setPage(Number(event.target.value))
    }

    function updateByPerPage(event) {
        setPerPage(Number(event.target.value))
    }

    function getUpdatedItemsList(event){
        fetchMore({
            variables:{
                page: page,
                perPage: perPage
            },
            updateQuery:(prevResults, {fetchMoreResult}) => {
                return (fetchMoreResult) ? fetchMoreResult : prevResults
            }
        }) 
    }

    return(
        <div>
            <form>
                Page: <input name="page" value={page} defaultValue={page} onChange={updateByPage} /> 
                Items per page: <input name="perPage" value={perPage} defaultValue={perPage} onChange={updateByPerPage}/>
            </form>
            <button onClick={getUpdatedItemsList}>Refresh</button>

            <div>
                {data.items.map((item) => {
                    return <p>{item.absoluteIndex+1}    |   {item.id}   |   {item.name} |   {item.absoluteIndex} </p>
                }) }
            </div>
        </div>
    )
}

export default GetItems