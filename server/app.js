const express = require('express');
const axios = require('axios');
const cors = require('cors')
const { graphqlHTTP } = require('express-graphql');
const { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLList, GraphQLSchema } = require('graphql');

const typeDef = new GraphQLObjectType({
  name: 'Items',
  fields: {
    id:{
      type: GraphQLString
    },
    absoluteIndex:{
      type:  GraphQLInt
    },
    name: {
      type: GraphQLString
    }
  }
})

const rootQuery = new GraphQLObjectType({
  name: 'rootQuery',
  fields: {
    items:{
      type: GraphQLList(typeDef),
      args: {
        page: { 
          type: GraphQLInt 
        },
        perPage: { 
          type: GraphQLInt 
        }
      },
      async resolve (_, {page, perPage}) {
        let data = []

        let startsFrom = page * perPage - perPage
        let endsWith = startsFrom + perPage

        if (endsWith > 100) {
          let required = Math.trunc(endsWith / 100 + 1)
          let reqPage = 1
          while (reqPage <= required){
            let response = await axios.get(`https://sf-legacy-api.vercel.app/items?page=${reqPage}`)
            data.push(...response.data.data)
            reqPage++
          }
        } else {
          let response = await axios.get(`https://sf-legacy-api.vercel.app/items?page=1`)
          data = response.data.data
        }
        
        let out = data.slice(startsFrom,endsWith)
        return out
      }
    },
  }
})

const schema = new GraphQLSchema({ query: rootQuery })

const app = express();

app.use('/items', cors(), graphqlHTTP({
  schema,
  graphiql: true
}));
app.listen(8123);

console.log(`🚀 Server ready at http://localhost:8123/items`);